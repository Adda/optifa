#!/usr/bin/env sh

# file name: run_tests.sh
#
# Script to run a single test.
#
# project: Abstraction of State Languages in Automata Algorithms
#
# author: David Chocholatý (xchoch08), FIT BUT

export POSIXLY_CORRECT=yes

python3 plot_graph_comparison_basic_length_abstraction_time_difference.py   ../../results/combined_results_appended.csv
python3 plot_graph_comparison_combined_sats.py ../../results/combined_results_appended.csv
python3 plot_graph_comparison_combined_time_difference.py ../../results/combined_results_appended.csv
python3 plot_graph_comparison_length_abstraction_parikh_image.py ../../results/combined_results_appended.csv
python3 plot_graph_comparison_length_abstraction_smt_difference.py ../../results/combined_results_appended.csv
python3 plot_graph_pi_abstraction_time_difference.py ../../results/combined_results_appended.csv
python3 plot_graph_pi_combined_abstraction_time_difference.py  ../../results/combined_results_appended.csv
python3 plot_graph_pruning_length_abstraction.py ../../results/combined_results_appended.csv
python3 plot_graph_pruning_length_abstraction_modifications.py ../../results/modification_et_res.csv ../../results/modification_fp_res.csv
python3 plot_graph_pruning_length_abstraction_modifications_lasso.py ../../results/modification_et_res.csv ../../results/modification_fp_res.csv
python3 plot_graph_pruning_pi_abstraction.py ../../results/combined_results_appended.csv
python3 plot_parikh_image_graph.py ../../results/combined_results_appended.csv

# End of file.
